package southard.basiccollections;

import java.util.*;

public class Collections {

    public static void main(String[] args) {
        System.out.println("My List Example: Sunglass brands");
        List list = new ArrayList();
        list.add("Maui Jim");
        list.add("Oakley");
        list.add("Kate Spade");
        list.add("Ray-Ban");

        for (Object str : list) {
            System.out.println((String) str);
        }

        System.out.println("My set example: Sentence");
        Set set = new TreeSet();
        set.add("How");
        set.add("much");
        set.add("wood");
        set.add("would");
        set.add("a");
        set.add("woodchuck");
        set.add("chuck");


        for (Object str : set) {
            System.out.println((String) str);
        }

        System.out.println("My Queue Example: Sentence");
        Queue queue = new PriorityQueue();
        queue.add("How");
        queue.add("much");
        queue.add("wood");
        queue.add("would");
        queue.add("a");
        queue.add("woodchuck");
        queue.add("chuck");


        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());


        }

        System.out.println("My Map Example: Correct Sentence");
        Map map = new HashMap();
        map.put(1,"How");
        map.put(2,"much");
        map.put(3,"wood");
        map.put(4,"would");
        map.put(5,"a");
        map.put(6,"woodchuck");
        map.put(7,"chuck");

        for (int i = 1; i < 8; i++) {
            String result = (String)map.get(i);
            System.out.println(result);

        }

        System.out.println("My Generics List: TV show and Character");
        List<Shows> myList = new LinkedList<Shows>();
        myList.add(new Shows( "Greys Anatomy", "Merideth"));
        myList.add(new Shows( "Smallville", "Clark"));
        myList.add(new Shows( "Rick and Morty","Morty"));
        myList.add(new Shows( "The Walking Dead", "Rick"));


        for (Shows show : myList) {
            System.out.println(show);
        }
    }
}
