package southard.basiccollections;

public class Shows {
    private String title;
    private String character;

    public Shows(String title, String character) {
        this.title = title;
        this.character = character;

    }

    public String toString() {
        return "Title: " + title + " has a main character called: " + character;
    }
}
