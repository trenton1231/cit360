package JSON;

public class Patient {
    private String firstName;
    private String lastName;
    private long phone;

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public long getPhone() {
        return phone;
    }
    public void setCellPhone(long phone) {
        this.phone = phone;
    }
    //public String toString() {
      //  return "First Name: " + firstName + " Last Name: " + lastName + " Cell: " + phone;
    //}
    public String toString() {
        return "Full Name: " + firstName + " " + lastName + " Contact: " + phone;
    }
}
