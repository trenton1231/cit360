package JSON;
// Import needed utilities from jackson library
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
//establish our class
public class JSON {
    //setup our JSON info to enable converting of our info to a JSON string
    public static String patientToJSON (Patient Patient) {

        ObjectMapper mapper = new ObjectMapper();
        String a = "";

        try {
            a = mapper.writeValueAsString(Patient);
        } catch (JsonProcessingException error) {
            System.err.println(error.toString());
        }

        return a;
    }
    //Setup our JSON to output our input to look nice
    public static Patient JSONtoPatient (String a) {

        ObjectMapper mapper = new ObjectMapper();
        Patient Patient = null;

        try {
            Patient = mapper.readValue(a, Patient.class);
        } catch (JsonProcessingException error) {
            System.err.println(error.toString());
        }
        return Patient;
    }

    public static void main(String[] args) {
        //Give our JSON some info to use for output
        Patient pat = new Patient();
        pat.setFirstName("Billy");
        pat.setLastName("Bojangles");
        pat.setCellPhone(1234567890);

        String json = JSON.patientToJSON(pat);
        System.out.println(json);

        Patient pat2 = JSON.JSONtoPatient(json);
        System.out.println(pat2);
    }

}
