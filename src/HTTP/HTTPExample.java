package HTTP;

import java.net.*;
import java.io.*;
import java.util.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;


public class HTTPExample {
    //Pulling HTTP content from URL
    public static String getHttpContent(String string) {

        String content="";

        try {
            URL url = new URL(string);

        } catch (Exception error) {
            System.err.println(error.toString());
        }

        return content;

    }
    //Pulling headers from URL data
    public static Map getHTTPHeaders(String string) {
        Map hashmap = null;

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            hashmap = http.getHeaderFields();
        } catch (Exception e) {
            System.err.println(e.toString());
        }
        return hashmap;
    }


    public static void main(String[] args) throws MalformedURLException {
        //Print out the entire content of HTTP from specified URL
        System.out.println(HTTPExample.getHttpContent("https://www.gamingtribe.com/page/GTribe"));
        //Pulling out the headers for a HTTP site
        Map<Integer, String> m = HTTPExample.getHTTPHeaders("https://www.gamingtribe.com/page/GTribe");
        //Print out website we are pulling our data from
        System.out.println("\n" + "https://www.gamingtribe.com/page/GTribe");
        //Run until there are no headers left
        for (Map.Entry<Integer, String> entry : m.entrySet()) {
            System.out.println("Key= " + entry.getKey());
        }
        //prompt user to input a URL
        Scanner url = new Scanner(System.in);
        System.out.println("Enter your URL");
        String ur2 = url.nextLine();
        //convert user input to URL in order to collect data
        URL ur3 = new URL(ur2);
        //Display back out data about user submitted URL
        System.out.println("\n" + ur3.toString());
        System.out.println("Protocol: - " + ur3.getProtocol());
        System.out.println("Hostname:= " + ur3.getHost());
        System.out.println("Default port:- " + ur3.getDefaultPort());
        System.out.println("Query:- " + ur3.getQuery());
        System.out.println("Path:- " + ur3.getPath());
        System.out.println("File:- " + ur3.getFile());
    }


}
